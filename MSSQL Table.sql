﻿-- Players table
CREATE TABLE Players (
    Id INT PRIMARY KEY IDENTITY,
    Name NVARCHAR(50),
    Lucky BIT,
    Points INT
);

-- Items table
CREATE TABLE Items (
    Id INT PRIMARY KEY IDENTITY,
    Name NVARCHAR(50),
    Description NVARCHAR(200),
    IsFound BIT,
    IsUsed BIT
);

-- Levels table
CREATE TABLE Levels (
    Id INT PRIMARY KEY IDENTITY,
    Name NVARCHAR(50),
    Description NVARCHAR(300),
    Riddle NVARCHAR(300),
    Solution NVARCHAR(100),
    IsSolved BIT,
    ItemId INT,
    FOREIGN KEY (ItemId) REFERENCES Items(Id)
);

-- PlayerItems table
CREATE TABLE PlayerItems (
    Id INT PRIMARY KEY IDENTITY,
    PlayerId INT,
    ItemId INT,
    FOREIGN KEY (PlayerId) REFERENCES Players(Id),
    FOREIGN KEY (ItemId) REFERENCES Items(Id)
);

-- Insert startup data
INSERT INTO [dbo].[Items] ([Name], [Description], [IsFound], [IsUsed])
VALUES
('watch', 'A mint condition wristwatch with its hand stuck on one minute past 12. Found in the Absolute Silence.', 0, 0),
('map', 'A bizarre map filled with unseen continents. Found in Crimson Flames.', 0, 0),
('sponge', 'An ordinary kitchen sponge. Why is it here..? Found in Shifting Sands.', 0, 0),
('nothing', 'You could not find anything useful.', 0, 0)

INSERT INTO [dbo].[Levels] ([Name], [Description], [Riddle], [Solution], [IsSolved], [ItemId])
VALUES
('Verdant Hues', 'Charm fades to damp decay. Glowing fungi paint grotesque shapes on the watchful forest, alive with unseen creatures.', 'I have cities, but no houses. I have mountains, but no trees. I have water, but no fish. What am I?', 'map', 0, 4),
('Shifting Sands', 'Mirage solidifies. Howling wind whips sand, baking sun beats down. Skeletal remains jut from endless dunes, while a lone palm tree whispers in the wind.', 'I am always coming, but never arrive. I am always present, but never here. What am I?', 'tomorrow', 0, 3),
('Crimson Flames', 'Smoke chokes the chamber. Jagged cracks glow with residual heat. Blackened remains litter the floor, and a charred inscription hides secrets in the ash.', 'I have a neck without a head, a body without legs, and I can be found in water, but am not wet. What am I?', 'bottle', 0, 2),
('Eerie Whispers', 'Silence replaces murmurs. Grotesque carvings writhe in flickering light. Dust and decay hang thick, oppressive darkness presses in.', 'What has to be broken before you can use it?', 'egg', 0, 4),
('Absolute Silence', 'Solid blackness surrounds. Cold stone chills your feet. Deafening silence pushes against your eardrums, endless darkness offers no escape.', 'I am full of holes, but can still hold water. What am I?', 'sponge', 0, 1)

-- Check data
SELECT * From Players
SELECT * From Items
SELECT * From Levels
SELECT * From PlayerItems

-- Drop tables
DROP TABLE PlayerItems
DROP TABLE Levels
DROP TABLE Players
DROP TABLE Items

-- Check if Identity column is set
SELECT COLUMN_NAME, COLUMNPROPERTY(OBJECT_ID('dbo.Levels'), COLUMN_NAME, 'IsIdentity') AS IsIdentity
FROM INFORMATION_SCHEMA.COLUMNS
WHERE TABLE_SCHEMA = 'dbo' -- This is assuming you're using the default schema
  AND TABLE_NAME = 'Levels';