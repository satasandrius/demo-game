# Void Escape - Demo version

<div align="center">
![Void_Escape](https://media.licdn.com/dms/image/D4D2DAQEDqy5hCBIH_g/profile-treasury-image-shrink_800_800/0/1712827134016?e=1713434400&v=beta&t=dC6SS1nmrg6hyuVq6_fnCKBRau-kBBBjD-UJbHSXbJs)
</div>

Demo version of the text-based game Void Escape.
The aim of the game is to solve the puzzles and escape from the mysterious room.
The project is the last assignment of the Vilnius Coding School course.

## Functionality

The user will be able to create his character, give him/her a name and assign a "success" value after answering a question.

The user will access the game through the console application and will be able to solve puzzles by typing text on the keyboard.
The console application will retrieve information from the database through the developed .NET Core Web API. User actions will change data in the database.

Everytime a new player is created - the game resets, resetting items and levels values to default values.

## Getting started
To run the project locally, the following steps are required:
1. Install .NET 8.0 (https://dotnet.microsoft.com/download).
2. Install the required packages:
- Microsoft.EntityFrameworkCore;
- Swashbuckle.AspNetCore;
- Moq;
- Serilog for logging (logs are written to a text file and console).
3. Set up a MSSQL database. For this project, a local database is used.  
4. Update the `appsettings.json` file with your MSSQL connection string.
5. Update the GameApiUrl in `config.json` file if needed. File is found in GameLauncher folder.
6. Run the application.

## SQL Scripts
```
-- Players table
CREATE TABLE Players (
    Id INT PRIMARY KEY IDENTITY,
    Name NVARCHAR(50),
    Lucky BIT,
    Points INT
);

-- Items table
CREATE TABLE Items (
    Id INT PRIMARY KEY IDENTITY,
    Name NVARCHAR(50),
    Description NVARCHAR(200),
    IsFound BIT,
    IsUsed BIT
);

-- Levels table
CREATE TABLE Levels (
    Id INT PRIMARY KEY IDENTITY,
    Name NVARCHAR(50),
    Description NVARCHAR(300),
    Riddle NVARCHAR(300),
    Solution NVARCHAR(100),
    IsSolved BIT,
    ItemId INT,
    FOREIGN KEY (ItemId) REFERENCES Items(Id)
);

-- PlayerItems table
CREATE TABLE PlayerItems (
    Id INT PRIMARY KEY IDENTITY,
    PlayerId INT,
    ItemId INT,
    FOREIGN KEY (PlayerId) REFERENCES Players(Id),
    FOREIGN KEY (ItemId) REFERENCES Items(Id)
);

-- Insert startup data
INSERT INTO [dbo].[Items] ([Name], [Description], [IsFound], [IsUsed])
VALUES
('watch', 'A mint condition wristwatch with its hand stuck on one minute past 12. Found in the Absolute Silence.', 0, 0),
('map', 'A bizarre map filled with unseen continents. Found in Crimson Flames.', 0, 0),
('sponge', 'An ordinary kitchen sponge. Why is it here..? Found in Shifting Sands.', 0, 0),
('nothing', 'You could not find anything useful.', 0, 0)

INSERT INTO [dbo].[Levels] ([Name], [Description], [Riddle], [Solution], [IsSolved], [ItemId])
VALUES
('Verdant Hues', 'Charm fades to damp decay. Glowing fungi paint grotesque shapes on the watchful forest, alive with unseen creatures.', 'I have cities, but no houses. I have mountains, but no trees. I have water, but no fish. What am I?', 'map', 0, 4),
('Shifting Sands', 'Mirage solidifies. Howling wind whips sand, baking sun beats down. Skeletal remains jut from endless dunes, while a lone palm tree whispers in the wind.', 'I am always coming, but never arrive. I am always present, but never here. What am I?', 'tomorrow', 0, 3),
('Crimson Flames', 'Smoke chokes the chamber. Jagged cracks glow with residual heat. Blackened remains litter the floor, and a charred inscription hides secrets in the ash.', 'I have a neck without a head, a body without legs, and I can be found in water, but am not wet. What am I?', 'bottle', 0, 2),
('Eerie Whispers', 'Silence replaces murmurs. Grotesque carvings writhe in flickering light. Dust and decay hang thick, oppressive darkness presses in.', 'What has to be broken before you can use it?', 'egg', 0, 4),
('Absolute Silence', 'Solid blackness surrounds. Cold stone chills your feet. Deafening silence pushes against your eardrums, endless darkness offers no escape.', 'I am full of holes, but can still hold water. What am I?', 'sponge', 0, 1)

```


## DTOs
DTOs used in this solution improves performance by reducing the amount of data that needs to be transferred, and increases flexibility for future updates, as well as helps maintain the separation of concerns in the application by preventing the client from being exposed to the internal details of the entities.

## Middleware
The TimingMiddleware is a middleware component in a game API that measures the time it takes for requests to be processed.

## Decorators
The LoggingPlayerRepositoryDecorator class is a decorator that adds logging functionality to a repository that handles Player entities.

## Known bugs

- So far (items) can be found, but not used. Now finding an item gives a hint of an answer to the riddle.
- Riddles can be solved, but the player can't accumulate points for solutions, therefore the player can't finish the game and escape the Void.
- If the player exits to the main menu, he can create new player, but once the player tries to pick up the same items as in the last playthrough - the game crashes.

## Authors and acknowledgment
Creator: Andrius Šatas.