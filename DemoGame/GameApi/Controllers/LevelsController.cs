﻿using BusinessLogic.Parameters.Levels;
using BusinessLogic.Services;
using BusinessLogic.Services.Levels;
using DataAccess.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Shared.Dto.Levels;

namespace GameApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LevelsController : ControllerBase
    {
        private readonly IService<CreateLevelParameter, LevelDto> _createLevelService;
        private readonly IService<GetAllLevelsParameter, List<LevelDto>> _getAllLevelsService;
        private readonly IService<GetOneLevelParameter, LevelDto> _getOneLevelService;
        private readonly IService<UpdateLevelParameter, LevelDto> _updateLevelService;
        private readonly IService<DeleteLevelParameter, LevelDto> _deleteLevelService;

        public LevelsController(
            IService<CreateLevelParameter, LevelDto> createLevelService,
            IService<GetAllLevelsParameter, List<LevelDto>> getAllLevelsService,
            IService<GetOneLevelParameter, LevelDto> getOneLevelService,
            IService<UpdateLevelParameter, LevelDto> updateLevelService,
            IService<DeleteLevelParameter, LevelDto> deleteLevelService)
        {
            _createLevelService = createLevelService;
            _getAllLevelsService = getAllLevelsService;
            _getOneLevelService = getOneLevelService;
            _updateLevelService = updateLevelService;
            _deleteLevelService = deleteLevelService;
        }

        [HttpPost]
        public async Task<ActionResult<LevelDto>> CreateAsync(CreateLevelDto level)
        {
            var result = await _createLevelService.CallAsync(new CreateLevelParameter(level));

            return StatusCode(StatusCodes.Status201Created, result);
        }

        [HttpGet]
        public async Task<ActionResult<LevelDto>> GetAllAsync()
        {
            var result = await _getAllLevelsService.CallAsync(new GetAllLevelsParameter(10));

            return StatusCode(StatusCodes.Status200OK, result);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<LevelDto>> GetOneAsync(int id)
        {
            var result = await _getOneLevelService.CallAsync(new GetOneLevelParameter(id));

            return StatusCode(StatusCodes.Status200OK, result);
        }

        [HttpPut("{id}")]
        public async Task<ActionResult<LevelDto>> UpdateAsync(int id, UpdateLevelDto level)
        {
            if (id != level.Id)
            {
                return StatusCode(StatusCodes.Status400BadRequest);
            }

            var result = await _updateLevelService.CallAsync(new UpdateLevelParameter(level));

            return StatusCode(StatusCodes.Status200OK, result);
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<LevelDto>> DeleteAsync(int id)
        {
            var result = await _deleteLevelService.CallAsync(new DeleteLevelParameter(id));

            return StatusCode(StatusCodes.Status200OK, result);
        }
    }
}
