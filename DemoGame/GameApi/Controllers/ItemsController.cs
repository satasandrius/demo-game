﻿using BusinessLogic.Parameters.Items;
using BusinessLogic.Services;
using Microsoft.AspNetCore.Mvc;
using Shared.Dto.Items;

namespace GameApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ItemsController : ControllerBase
    {
        private readonly IService<CreateItemParameter, ItemDto> _createItemService;
        private readonly IService<GetAllItemsParameter, List<ItemDto>> _getAllItemsService;
        private readonly IService<GetOneItemParameter, ItemDto> _getOneItemService;
        private readonly IService<UpdateItemParameter, ItemDto> _updateItemService;
        private readonly IService<DeleteItemParameter, ItemDto> _deleteItemService;

        public ItemsController(
            IService<CreateItemParameter, ItemDto> createItemService,
            IService<GetAllItemsParameter, List<ItemDto>> getAllItemsService,
            IService<GetOneItemParameter, ItemDto> getOneItemService,
            IService<UpdateItemParameter, ItemDto> updateItemService,
            IService<DeleteItemParameter, ItemDto> deleteItemService)
        {
            _createItemService = createItemService;
            _getAllItemsService = getAllItemsService;
            _getOneItemService = getOneItemService;
            _updateItemService = updateItemService;
            _deleteItemService = deleteItemService;
        }

        [HttpPost]
        public async Task<ActionResult<ItemDto>> CreateAsync(CreateItemDto item)
        {
            var result = await _createItemService.CallAsync(new CreateItemParameter(item));

            return StatusCode(StatusCodes.Status201Created, result);
        }

        [HttpGet]
        public async Task<ActionResult<ItemDto>> GetAllAsync()
        {
            var result = await _getAllItemsService.CallAsync(new GetAllItemsParameter(10));

            return StatusCode(StatusCodes.Status200OK, result);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<ItemDto>> GetOneAsync(int id)
        {
            var result = await _getOneItemService.CallAsync(new GetOneItemParameter(id));

            return StatusCode(StatusCodes.Status200OK, result);
        }

        [HttpPut("{id}")]
        public async Task<ActionResult<ItemDto>> UpdateAsync(int id, UpdateItemDto item)
        {
            if (id != item.Id)
            {
                return StatusCode(StatusCodes.Status400BadRequest);
            }
            
            var result = await _updateItemService.CallAsync(new UpdateItemParameter(item));

            return StatusCode(StatusCodes.Status200OK, result);
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<ItemDto>> DeleteAsync(int id)
        {
            var result = await _deleteItemService.CallAsync(new DeleteItemParameter(id));

            return StatusCode(StatusCodes.Status200OK, result);
        }
    }
}
