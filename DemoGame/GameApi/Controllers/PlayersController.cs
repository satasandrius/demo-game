﻿using BusinessLogic.Parameters.Players;
using BusinessLogic.Services;
using DataAccess;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Shared.Dto.Players;
using Swashbuckle.AspNetCore.Annotations;

namespace GameApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PlayersController : ControllerBase
    {
        private readonly IService<CreatePlayerParameter, PlayerDto> _createPlayerService;
        private readonly IService<GetAllPlayersParameter, List<PlayerDto>> _getAllPlayersService;
        private readonly IService<GetOnePlayerParameter, PlayerDto> _getOnePlayerService;
        private readonly IService<UpdatePlayerParameter, PlayerDto> _updatePlayerService;
        private readonly IService<DeletePlayerParameter, PlayerDto> _deletePlayerService;

        public PlayersController(
            IService<CreatePlayerParameter, PlayerDto> createPlayerService,
            IService<GetAllPlayersParameter, List<PlayerDto>> getAllPlayersService,
            IService<GetOnePlayerParameter, PlayerDto> getOnePlayerService,
            IService<UpdatePlayerParameter, PlayerDto> updatePlayerService,
            IService<DeletePlayerParameter, PlayerDto> deletePlayerService)
        {
            _createPlayerService = createPlayerService;
            _getAllPlayersService = getAllPlayersService;
            _getOnePlayerService = getOnePlayerService;
            _updatePlayerService = updatePlayerService;
            _deletePlayerService = deletePlayerService;
        }

        [HttpPost]
        [SwaggerOperation(Summary = "Creates a new player.")]
        public async Task<ActionResult<PlayerDto>> CreateAsync(CreatePlayerDto player)
        {
            var result = await _createPlayerService.CallAsync(new CreatePlayerParameter(player));

            return StatusCode(StatusCodes.Status201Created, result);
        }

        [HttpGet]
        [SwaggerOperation(Summary = "Gets all players.")]
        public async Task<ActionResult<PlayerDto>> GetAllAsync()
        {
            var result = await _getAllPlayersService.CallAsync(new GetAllPlayersParameter(10));

            return StatusCode(StatusCodes.Status200OK, result);
        }

        [HttpGet("{id}")]
        [SwaggerOperation(Summary = "Gets one player by ID.")]
        public async Task<ActionResult<PlayerDto>> GetOneAsync(int id)
        {
            var result = await _getOnePlayerService.CallAsync(new GetOnePlayerParameter(id));

            return StatusCode(StatusCodes.Status200OK, result);
        }

        [HttpPut("{id}")]
        [SwaggerOperation(Summary = "Updates one player by ID.")]
        public async Task<ActionResult<PlayerDto>> UpdateAsync(int id, UpdatePlayerDto player)
        {
            if (id != player.Id)
            {
                return StatusCode(StatusCodes.Status400BadRequest);
            }

            var result = await _updatePlayerService.CallAsync(new UpdatePlayerParameter(player));

            return StatusCode(StatusCodes.Status200OK, result);
        }

        [HttpDelete("{id}")]
        [SwaggerOperation(Summary = "Deletes one player by ID.")]
        public async Task<ActionResult<PlayerDto>> DeleteAsync(int id)
        {
            var result = await _deletePlayerService.CallAsync(new DeletePlayerParameter(id));

            return StatusCode(StatusCodes.Status200OK, result);
        }
    }
}
