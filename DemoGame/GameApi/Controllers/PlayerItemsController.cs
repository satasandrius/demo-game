﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace GameApi.Controllers
{
    using BusinessLogic.Parameters.PlayerItems;
    using BusinessLogic.Services;
    using Microsoft.AspNetCore.Mvc;
    using Shared.Dto.PlayerItems;

    namespace GameApi.Controllers
    {
        [Route("api/[controller]")]
        [ApiController]
        public class PlayerItemsController : ControllerBase
        {
            private readonly IService<CreatePlayerItemParameter, PlayerItemDto> _createPlayerItemService;
            private readonly IService<GetAllPlayerItemsParameter, List<PlayerItemDto>> _getAllPlayerItemsService;
            private readonly IService<GetOnePlayerItemParameter, PlayerItemDto> _getOnePlayerItemService;
            private readonly IService<UpdatePlayerItemParameter, PlayerItemDto> _updatePlayerItemService;
            private readonly IService<DeletePlayerItemParameter, PlayerItemDto> _deletePlayerItemService;

            public PlayerItemsController(
                IService<CreatePlayerItemParameter, PlayerItemDto> createPlayerItemService,
                IService<GetAllPlayerItemsParameter, List<PlayerItemDto>> getAllPlayerItemsService,
                IService<GetOnePlayerItemParameter, PlayerItemDto> getOnePlayerItemService,
                IService<UpdatePlayerItemParameter, PlayerItemDto> updatePlayerItemService,
                IService<DeletePlayerItemParameter, PlayerItemDto> deletePlayerItemService)
            {
                _createPlayerItemService = createPlayerItemService;
                _getAllPlayerItemsService = getAllPlayerItemsService;
                _getOnePlayerItemService = getOnePlayerItemService;
                _updatePlayerItemService = updatePlayerItemService;
                _deletePlayerItemService = deletePlayerItemService;
            }

            [HttpPost]
            public async Task<ActionResult<PlayerItemDto>> CreateAsync(CreatePlayerItemDto playerItem)
            {
                var result = await _createPlayerItemService.CallAsync(new CreatePlayerItemParameter(playerItem));

                return StatusCode(StatusCodes.Status201Created, result);
            }

            [HttpGet]
            public async Task<ActionResult<PlayerItemDto>> GetAllAsync()
            {
                var result = await _getAllPlayerItemsService.CallAsync(new GetAllPlayerItemsParameter(10));

                return StatusCode(StatusCodes.Status200OK, result);
            }

            [HttpGet("{id}")]
            public async Task<ActionResult<PlayerItemDto>> GetOneAsync(int id)
            {
                var result = await _getOnePlayerItemService.CallAsync(new GetOnePlayerItemParameter(id));

                return StatusCode(StatusCodes.Status200OK, result);
            }

            [HttpPut("{id}")]
            public async Task<ActionResult<PlayerItemDto>> UpdateAsync(int id, UpdatePlayerItemDto playerItem)
            {
                if (id != playerItem.Id)
                {
                    return StatusCode(StatusCodes.Status400BadRequest);
                }

                var result = await _updatePlayerItemService.CallAsync(new UpdatePlayerItemParameter(playerItem));

                return StatusCode(StatusCodes.Status200OK, result);
            }

            [HttpDelete("{id}")]
            public async Task<ActionResult<PlayerItemDto>> DeleteAsync(int id)
            {
                var result = await _deletePlayerItemService.CallAsync(new DeletePlayerItemParameter(id));

                return StatusCode(StatusCodes.Status200OK, result);
            }
        }

    }

}
