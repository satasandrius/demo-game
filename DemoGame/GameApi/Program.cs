﻿using BusinessLogic.Parameters.Items;
using BusinessLogic.Parameters.Levels;
using BusinessLogic.Parameters.PlayerItems;
using BusinessLogic.Parameters.Players;
using BusinessLogic.Services;
using BusinessLogic.Services.Items;
using BusinessLogic.Services.Levels;
using BusinessLogic.Services.PlayerItems;
using BusinessLogic.Services.Players;
using DataAccess;
using DataAccess.Entities;
using DataAccess.Repositories;
using GameApi.Middlewares;
using Microsoft.EntityFrameworkCore;
using Serilog;
using Shared.Dto.Items;
using Shared.Dto.Levels;
using Shared.Dto.PlayerItems;
using Shared.Dto.Players;

namespace GameApi
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            // Add services to the container.
            builder.Services.AddDbContext<GameDbContext>(
                options => options.UseSqlServer(builder.Configuration.GetConnectionString("DefaultConnection")));

            Log.Logger = new LoggerConfiguration()
                .WriteTo.Console()
                .MinimumLevel.Information()
                .WriteTo.File("logs/logs.txt", rollingInterval: RollingInterval.Day)
                //.Filter.ByExcluding(Matching.FromSource("namespace")) - jei vėliau nuspręsiu kažko nelogint.
                .CreateLogger();

            builder.Host.UseSerilog();

            builder.Services.AddControllers();

            #region Item services
            builder.Services.AddTransient<IService<CreateItemParameter, ItemDto>, CreateItemService>();
            builder.Services.AddTransient<IService<GetAllItemsParameter, List<ItemDto>>, GetAllItemsService>();
            builder.Services.AddTransient<IService<GetOneItemParameter, ItemDto>, GetOneItemService>();
            builder.Services.AddTransient<IService<UpdateItemParameter, ItemDto>, UpdateItemService>();
            builder.Services.AddTransient<IService<DeleteItemParameter, ItemDto>, DeleteItemService>();
            #endregion

            #region Level services
            builder.Services.AddTransient<IService<CreateLevelParameter, LevelDto>, CreateLevelService>();
            builder.Services.AddTransient<IService<GetAllLevelsParameter, List<LevelDto>>, GetAllLevelsService>();
            builder.Services.AddTransient<IService<GetOneLevelParameter, LevelDto>, GetOneLevelService>();
            builder.Services.AddTransient<IService<UpdateLevelParameter, LevelDto>, UpdateLevelService>();
            builder.Services.AddTransient<IService<DeleteLevelParameter, LevelDto>, DeleteLevelService>();
            #endregion

            #region PlayerItem services
            builder.Services.AddTransient<IService<CreatePlayerItemParameter, PlayerItemDto>, CreatePlayerItemService>();
            builder.Services.AddTransient<IService<GetAllPlayerItemsParameter, List<PlayerItemDto>>, GetAllPlayerItemsService>();
            builder.Services.AddTransient<IService<GetOnePlayerItemParameter, PlayerItemDto>, GetOnePlayerItemService>();
            builder.Services.AddTransient<IService<UpdatePlayerItemParameter, PlayerItemDto>, UpdatePlayerItemService>();
            builder.Services.AddTransient<IService<DeletePlayerItemParameter, PlayerItemDto>, DeletePlayerItemService>();
            #endregion

            #region Player services
            builder.Services.AddTransient<IService<CreatePlayerParameter, PlayerDto>, CreatePlayerService>();
            builder.Services.AddTransient<IService<GetAllPlayersParameter, List<PlayerDto>>, GetAllPlayersService>();
            builder.Services.AddTransient<IService<GetOnePlayerParameter, PlayerDto>, GetOnePlayerService>();
            builder.Services.AddTransient<IService<UpdatePlayerParameter, PlayerDto>, UpdatePlayerService>();
            builder.Services.AddTransient<IService<DeletePlayerParameter, PlayerDto>, DeletePlayerService>();
            #endregion

            #region Repositories
            builder.Services.AddTransient<IRepository<Item>, ItemRepository>();
            builder.Services.AddTransient<IRepository<Level>, LevelRepository>();
            builder.Services.AddTransient<IRepository<Player>, PlayerRepository>();
            builder.Services.AddTransient<IRepository<PlayerItem>, PlayerItemRepository>();
            #endregion

            // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSwaggerGen(
                options =>
                {
                    options.SwaggerDoc(
                        "v1",
                        new Microsoft.OpenApi.Models.OpenApiInfo()
                        {
                            Title = "Void Escape",
                            Version = "Demo",
                            Description = "An example of a ASP.NET Core Web API used for a text game.",
                            Contact = new Microsoft.OpenApi.Models.OpenApiContact()
                            {
                                Name = "Andrius",
                                Email = "satasandrius@gmail.com"
                            }
                        });
                });

            var app = builder.Build();

            // Configure the HTTP request pipeline.
            if (app.Environment.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI();
            }

            app.UseHttpsRedirection();

            app.UseAuthorization();

            app.UseMiddleware<TimingMiddleware>();

            app.MapControllers();

            app.Run();
        }
    }
}