﻿using System.Text;
using System.Text.Json;
using GameLauncher.Levels;
using GameLauncher.Handlers;
using Shared.Dto.Items;
using Shared.Dto.Levels;

namespace GameLauncher
{
    public static class GameUI
    {
        private static readonly HttpClient client = new HttpClient();

        public static async Task RunAsync()
        {
            var configurationProvider = new ConfigurationProvider();
            var configuration = configurationProvider.GetConfiguration();
            client.BaseAddress = new Uri(configuration.GameApiUrl);

            bool appRunning = true;

            while (appRunning)
            {
                Console.WriteLine("\nVoid Escape (Demo)");
                Console.WriteLine("1. Start new game");
                Console.WriteLine("2. Those, who tried\n");
                Console.WriteLine("9. Exit the game\n");
                Console.Write("Your choice: ");
                string choice = Console.ReadLine();

                switch (choice)
                {
                    case "1":
                        await PlayerCreationHandler.CreatePlayer();
                        await ResetValues();
                        await MainHall.RunAsync();

                        break;

                    case "2":
                        await ThoseWhoTriedHandler.GetPlayersAsync();

                        break;

                    case "9": // Exit the game
                        Console.WriteLine("Exiting the game. Until next time...");

                        appRunning = false;

                        break;

                    default:
                        Console.WriteLine("Can't do that. Choose one of the options below.");
                        break;
                }
            }
        }

        public static async Task ResetValues()
        {
            using (var httpClient = new HttpClient())
            {
                var configurationProvider = new ConfigurationProvider();
                var configuration = configurationProvider.GetConfiguration();
                var itemServiceUrl = $"{configuration.GameApiUrl}api/items";
                var levelServiceUrl = $"{configuration.GameApiUrl}api/levels";

                // Create and update items
                var items = new List<UpdateItemDto>
                    {
                        new UpdateItemDto { Id = 1, Name = "watch", Description = "A mint condition wristwatch with its hand stuck on one minute past 12. Found in the Absolute Silence.", IsFound = false, IsUsed = false },
                        new UpdateItemDto { Id = 2, Name = "map", Description = "A bizarre map filled with unseen continents. Found in Crimson Flames.", IsFound = false, IsUsed = false },
                        new UpdateItemDto { Id = 3, Name = "sponge", Description = "An ordinary kitchen sponge. Why is it here..? Found in Shifting Sands.", IsFound = false, IsUsed = false },
                        new UpdateItemDto { Id = 4, Name = "nothing", Description = "You couldn't find anything useful.", IsFound = false, IsUsed = false }
                    };

                foreach (var item in items)
                {
                    var json = JsonSerializer.Serialize(item);
                    var content = new StringContent(json, Encoding.UTF8, "application/json");
                    var response = await httpClient.PutAsync($"{itemServiceUrl}/{item.Id}", content);

                    if (!response.IsSuccessStatusCode)
                    {
                        Console.WriteLine($"Failed to update item with ID {item.Id}");
                    }
                }

                // Create and update levels
                var levels = new List<UpdateLevelDto>
                    {
                        new UpdateLevelDto { Id = 1, Name = "Verdant Hues", Description = "Charm fades to damp decay. Glowing fungi paint grotesque shapes on the watchful forest, alive with unseen creatures.", Riddle = "I have cities, but no houses. I have mountains, but no trees. I have water, but no fish. What am I?", Solution = "map", IsSolved = false, ItemId = 4 },
                        new UpdateLevelDto { Id = 2, Name = "Shifting Sands", Description = "Mirage solidifies. Howling wind whips sand, baking sun beats down. Skeletal remains jut from endless dunes, while a lone palm tree whispers in the wind.", Riddle = "I am always coming, but never arrive. I am always present, but never here. What am I?", Solution = "tomorrow", IsSolved = false, ItemId = 3 },
                        new UpdateLevelDto { Id = 3, Name = "Crimson Flames", Description = "Smoke chokes the chamber. Jagged cracks glow with residual heat. Blackened remains litter the floor, and a charred inscription hides secrets in the ash.", Riddle = "I have a neck without a head, a body without legs, and I can be found in water, but am not wet. What am I?", Solution = "bottle", IsSolved = false, ItemId = 2 },
                        new UpdateLevelDto { Id = 4, Name = "Eerie Whispers", Description = "Silence replaces murmurs. Grotesque carvings writhe in flickering light. Dust and decay hang thick, oppressive darkness presses in.", Riddle = "What has to be broken before you can use it?", Solution = "egg", IsSolved = false, ItemId = 4 },
                        new UpdateLevelDto { Id = 5, Name = "Absolute Silence", Description = "Solid blackness surrounds. Cold stone chills your feet. Deafening silence pushes against your eardrums, endless darkness offers no escape.", Riddle = "I am full of holes, but can still hold water. What am I?", Solution = "sponge", IsSolved = false, ItemId = 1 }
                    };

                foreach (var level in levels)
                {
                    var json = JsonSerializer.Serialize(level);
                    var content = new StringContent(json, Encoding.UTF8, "application/json");
                    var response = await httpClient.PutAsync($"{levelServiceUrl}/{level.Id}", content);

                    if (!response.IsSuccessStatusCode)
                    {
                        Console.WriteLine($"Failed to update level with ID {level.Id}");
                    }
                }
            }
        }
    }
}
