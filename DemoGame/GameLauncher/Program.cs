﻿namespace GameLauncher
{
    internal class Program
    {
        static async Task Main(string[] args)
        {
            await GameUI.RunAsync();
        }
    }
}
