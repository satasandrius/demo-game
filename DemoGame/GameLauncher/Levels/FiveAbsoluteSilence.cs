﻿using Shared.Dto.Items;
using Shared.Dto.Levels;
using Shared.Dto.PlayerItems;
using Shared.Dto.Players;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace GameLauncher.Levels
{
    public class FiveAbsoluteSilence : ILevel
    {
        private static readonly HttpClient client = new HttpClient();

        public static async Task RunAsync()
        {
            var configurationProvider = new ConfigurationProvider();
            var configuration = configurationProvider.GetConfiguration();
            client.BaseAddress = new Uri(configuration.GameApiUrl);

            bool running = true;
            while (running)
            {
                Console.WriteLine("--------");
                Console.WriteLine("You're at the Absolute Silence.\n");
                Console.WriteLine("What do you do?\n");
                Console.WriteLine("1. Look around");
                Console.WriteLine("2. Search");
                Console.WriteLine("3. Use item");
                Console.WriteLine("4. Solve riddle\n");

                Console.WriteLine("9. Go back to the Main Hall\n");
                Console.Write("Your choice: ");
                string choice = Console.ReadLine();

                switch (choice)
                {
                    case "1":
                        await new FiveAbsoluteSilence().LookAround();

                        break;

                    case "2":
                        await new FiveAbsoluteSilence().Search();

                        break;

                    case "3":
                        await new FiveAbsoluteSilence().UseItem();

                        break;

                    case "4":
                        await new FiveAbsoluteSilence().SolveRiddle();

                        break;

                    case "9":
                        Console.WriteLine("Going back...\n");
                        running = false;

                        break;

                    default:
                        Console.WriteLine("Can't do that. Choose one of the options below.");
                        Console.WriteLine("--------");

                        break;
                }
            }
        }

        public async Task LookAround()
        {
            try
            {
                var response = await client.GetAsync("api/Levels");

                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    var levels = JsonSerializer.Deserialize<List<LevelDto>>(content);

                    foreach (var level in levels)
                    {
                        if (level.Name == "Absolute Silence")
                        {
                            Console.WriteLine("--------");
                            Console.WriteLine($"{level.Description}");
                        }
                    }
                }
                else
                {
                    Console.WriteLine("Can't see anything...\n");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Something is wrong: {ex.Message}");
            }
        }

        public async Task Search()
        {
            try
            {
                var response = await client.GetAsync("api/Levels");

                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    var levels = JsonSerializer.Deserialize<List<LevelDto>>(content);

                    foreach (var level in levels)
                    {
                        if (level.ItemId == 1)
                        {
                            var itemResponse = await client.GetAsync($"api/Items/{level.ItemId}");

                            if (itemResponse.IsSuccessStatusCode)
                            {
                                var itemContent = await itemResponse.Content.ReadAsStringAsync();
                                var item = JsonSerializer.Deserialize<ItemDto>(itemContent);
                                Console.WriteLine("--------");
                                Console.WriteLine(item.Description);

                                await CreatePlayerItem(await GetLatestPlayerIdAsync(), level.ItemId);

                                UpdateItemIsFound(level.ItemId);
                            }
                            else
                            {
                                throw new Exception($"Failed to get item with ID {level.ItemId}");
                            }
                        }
                    }
                }
                else
                {
                    Console.WriteLine("Can't see anything...\n");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Something is wrong: {ex.Message}");
            }
        }

        public async Task<int> GetLatestPlayerIdAsync()
        {
            var response = await client.GetAsync("api/Players");
            if (response.IsSuccessStatusCode)
            {
                var json = await response.Content.ReadAsStringAsync();
                var players = JsonSerializer.Deserialize<List<PlayerDto>>(json);

                var latestPlayer = players.OrderByDescending(p => p.Id).FirstOrDefault();
                if (latestPlayer != null)
                {
                    return latestPlayer.Id;
                }
            }

            return 0;
        }

        public async Task CreatePlayerItem(int playerId, int itemId)
        {
            try
            {
                var playerItem = new CreatePlayerItemDto
                {
                    PlayerId = playerId,
                    ItemId = itemId
                };

                var response = await client.PostAsync("api/PlayerItems", new StringContent(JsonSerializer.Serialize(playerItem), Encoding.UTF8, "application/json"));

                if (response.IsSuccessStatusCode)
                {
                    Console.WriteLine("This item is now in your possesion.");
                }
                else
                {
                    Console.WriteLine("For some reason, you can't pick up the item.");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Something is wrong: {ex.Message}");
            }
        }

        public async Task UpdateItemIsFound(int itemId)
        {
            try
            {
                var itemResponse = await client.GetAsync($"api/Items/{itemId}");

                if (itemResponse.IsSuccessStatusCode)
                {
                    var itemContent = await itemResponse.Content.ReadAsStringAsync();
                    var item = JsonSerializer.Deserialize<ItemDto>(itemContent);

                    item.IsFound = true;

                    var updateResponse = await client.PutAsync($"api/Items/{itemId}", new StringContent(JsonSerializer.Serialize(item), Encoding.UTF8, "application/json"));

                    if (updateResponse.IsSuccessStatusCode)
                    {
                        Console.WriteLine("You place it in your pocket.");
                    }
                }
                else
                {
                    throw new Exception($"Failed to get item with ID {itemId}");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Something is wrong: {ex.Message}");
            }
        }

        public async Task UseItem()
        {
            Console.WriteLine("You don't have anything that would be useful here.");
        }

        public async Task SolveRiddle()
        {
            try
            {
                var response = await client.GetAsync("api/Levels");

                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    var levels = JsonSerializer.Deserialize<List<LevelDto>>(content);

                    foreach (var level in levels)
                    {
                        if (level.Name == "Absolute Silence")
                        {
                            Console.WriteLine("--------");
                            Console.WriteLine($"You can't hear anything... But in your mind forms a riddle!\n " +
                                $"{level.Riddle}\n");
                            Console.Write("What's your answer? ");
                            string answer = Console.ReadLine();

                            if (answer == level.Solution)
                            {
                                Console.WriteLine("Exactly! Good job, you've solved the riddle of the Absolute Silence.");
                                level.IsSolved = true;
                                await UpdatePlayerPoints();
                            }
                            else
                            {
                                Console.WriteLine("Interesting guess, but... That's not correct.");
                            }
                        }
                    }
                }
                else
                {
                    Console.WriteLine("Can't see anything...\n");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Something is wrong: {ex.Message}");
            }
        }

        public async Task UpdatePlayerPoints()
        {
            var response = await client.GetAsync("api/Players");
            if (response.IsSuccessStatusCode)
            {
                var json = await response.Content.ReadAsStringAsync();
                var players = JsonSerializer.Deserialize<List<PlayerDto>>(json);

                var latestPlayer = players.OrderByDescending(p => p.Id).FirstOrDefault();

                if (latestPlayer != null)
                {
                    latestPlayer.Points++;

                    var updatePlayerDto = new UpdatePlayerDto
                    {
                        Id = latestPlayer.Id,
                        Name = latestPlayer.Name,
                        Lucky = latestPlayer.Lucky,
                        Points = latestPlayer.Points
                    };

                    var jsonContent = new StringContent(
                        JsonSerializer.Serialize(updatePlayerDto),
                        Encoding.UTF8,
                        "application/json");

                    var updateResponse = await client.PutAsync($"api/Players/{latestPlayer.Id}", jsonContent);

                    if (!updateResponse.IsSuccessStatusCode)
                    {
                        Console.WriteLine("Failed to update player points.");
                    }
                }
            }
        }
    }
}
