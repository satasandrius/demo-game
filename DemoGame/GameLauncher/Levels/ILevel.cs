﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameLauncher.Levels
{
    public interface ILevel
    {
        Task LookAround();

        Task Search();

        Task UseItem();

        Task SolveRiddle();
    }
}
