﻿using BusinessLogic.Parameters.Players;
using Microsoft.AspNetCore.Mvc;
using Shared.Dto.Players;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace GameLauncher.Levels
{
    public class MainHall
    {
        private static readonly HttpClient client = new HttpClient();

        public static async Task RunAsync()
        {
            var configurationProvider = new ConfigurationProvider();
            var configuration = configurationProvider.GetConfiguration();
            client.BaseAddress = new Uri(configuration.GameApiUrl);

            bool running = true;
            while (running)
            {
                Console.WriteLine("--------");
                Console.WriteLine("You're at the Main Hall. Five doorways arch before you, each a swirling vortex of colors.\n");
                Console.WriteLine("What do you do?\n");
                Console.WriteLine("1. Look around");
                Console.WriteLine("2. Enter doorway of Verdant Hues");
                Console.WriteLine("3. Enter doorway of Shifting Sands");
                Console.WriteLine("4. Enter doorway of Crimson Flames");
                Console.WriteLine("5. Enter doorway of Eerie Whispers");
                Console.WriteLine("6. Enter doorway of Absolute Silence");
                Console.WriteLine("7. Solve riddle\n");

                Console.WriteLine("999. Go back to the main menu\n");
                Console.Write("Your choice: ");
                string choice = Console.ReadLine();

                switch (choice)
                {
                    case "1":

                        Console.WriteLine("--------");
                        Console.WriteLine("You see five distinct doorways around you:" +
                            "\r\nDoorway of Verdant Hues: Vines creep across the threshold, beckoning you into a verdant forest." +
                            "\r\nDoorway of Shifting Sands: The entrance shimmers, resembling a vast desert mirage." +
                            "\r\nDoorway of Crimson Flames: Flickering flames dance around the archway, daring you to enter." +
                            "\r\nDoorway of Eerie Whispers: An unsettling murmur emanates from within, urging you closer." +
                            "\r\nDoorway of Absolute Silence: An inky blackness awaits behind this silent portal.");

                        break;

                    case "2":
                        Console.WriteLine("You've passed the doorway of Verdant Hues.");
                        await OneVerdantHues.RunAsync();

                        break;

                    case "3":
                        Console.WriteLine("You've passed the doorway of Shifting Sands.");
                        await TwoShiftingSands.RunAsync();

                        break;

                    case "4":
                        Console.WriteLine("You've passed the doorway of Crimson Flames.");
                        await ThreeCrimsonFlames.RunAsync();

                        break;

                    case "5":
                        Console.WriteLine("You've passed the doorway of Eerie Whispers.");
                        await FourEerieWhispers.RunAsync();

                        break;

                    case "6":
                        Console.WriteLine("You've passed the doorway of Absolute Silence.");
                        await FiveAbsoluteSilence.RunAsync();

                        break;

                    case "7":
                        bool success = await Escape();

                        if (success)
                        {
                            Console.WriteLine("--------");
                            Console.WriteLine("Finally, the last riddle crumbles, and the answer resonates through the space. " +
                                "The swirling doorways dim, their colors fading. " +
                                "In the center, where you once stood, a tear in reality blossoms. " +
                                "It expands, revealing an inky blackness that writhes with unseen energy. " +
                                "\r\n\r\nFor a heartbeat, an impossible silence reigns. " +
                                "Then, with a sound like a sigh escaping the cosmos itself, the portal swallows you whole. " +
                                "The tear snaps shut, leaving behind only the faintest shimmer, a memory of the impossible doorway.");

                            Console.WriteLine("Press anything to leave...");
                            running = false;

                        }
                        else
                        {
                            Console.WriteLine("Something is missing... Maybe more answers can be found in other rooms?");
                        }

                        break;

                    case "999": // Go back
                        Console.WriteLine("Going back...");
                        running = false;

                        break;

                    default:
                        Console.WriteLine("Can't do that. Choose one of the options below.");
                        Console.WriteLine("--------");
                        break;
                }
            }
        }

        public static async Task<bool> Escape()
        {
            var response = await client.GetAsync("api/Players");
            if (response.IsSuccessStatusCode)
            {
                var json = await response.Content.ReadAsStringAsync();
                var players = JsonSerializer.Deserialize<List<PlayerDto>>(json);

                var latestPlayer = players.OrderByDescending(p => p.Id).FirstOrDefault();
                if (latestPlayer != null && latestPlayer.Points == 5)
                {
                    return true;
                }
            }

            return false;
        }
    }
}
