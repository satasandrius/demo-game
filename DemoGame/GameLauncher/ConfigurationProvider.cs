﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameLauncher
{
    public class ConfigurationProvider
    {
        public Configuration GetConfiguration()
        {
            var configJson = File.ReadAllText("config.json");
            var configuration = JsonConvert.DeserializeObject<Configuration>(configJson);

            return configuration;
        }
    }
}
