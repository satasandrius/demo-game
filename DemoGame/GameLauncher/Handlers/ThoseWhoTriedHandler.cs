﻿using Shared.Dto.Players;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace GameLauncher.Handlers
{
    public class ThoseWhoTriedHandler
    {
        private static readonly HttpClient client = new HttpClient();

        public static async Task GetPlayersAsync()
        {
            var configurationProvider = new ConfigurationProvider();
            var configuration = configurationProvider.GetConfiguration();
            client.BaseAddress = new Uri(configuration.GameApiUrl);

            var response = await client.GetAsync("api/Players");

            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                var players = JsonSerializer.Deserialize<List<PlayerDto>>(content);

                Console.WriteLine("--------");
                Console.WriteLine("Seen in the Void:");

                foreach (var player in players)
                {
                    Console.WriteLine($"{player.Name}");
                }
            }
            else
            {
                Console.WriteLine("No one has tried this. Yet...\n");
            }
        }
    }
}
