﻿using Shared.Dto.Players;
using System.Text;
using System.Text.Json;

namespace GameLauncher.Handlers
{
    public static class PlayerCreationHandler
    {
        public async static Task CreatePlayer()
        {
            using (var client = new HttpClient())
            {
                var configurationProvider = new ConfigurationProvider();
                var configuration = configurationProvider.GetConfiguration();
                client.BaseAddress = new Uri(configuration.GameApiUrl);

                Console.WriteLine("\nEnter your name:");
                string playerName = Console.ReadLine();

                Console.WriteLine("\nWelcome, " + playerName + ". Tell me, what happens when you drop a sandwich?\n");
                Console.WriteLine("1. It gracefully falls onto my plate.");
                Console.WriteLine("2. It falls face down on the ground.\n");
                int playerLuckAnswer = int.Parse(Console.ReadLine());

                bool isLucky = false;
                bool isValidAnswer = false;
                do
                {
                    if (playerLuckAnswer == 1)
                    {
                        isLucky = true;
                        Console.WriteLine("\nLucky you!\n");
                        isValidAnswer = true;
                    }
                    else if (playerLuckAnswer == 2)
                    {
                        isLucky = false;
                        Console.WriteLine("\nNot so lucky, eh?\n");
                        isValidAnswer = true;
                    }
                    else
                    {
                        Console.WriteLine("1 or 2? Simple answer. Why don't we try again?\n");
                        playerLuckAnswer = int.Parse(Console.ReadLine());
                    }
                } while (!isValidAnswer);

                var createPlayerDto = new CreatePlayerDto
                {
                    Name = playerName,
                    Lucky = isLucky,
                    Points = 0
                };

                var json = JsonSerializer.Serialize(createPlayerDto);
                var content = new StringContent(json, Encoding.UTF8, "application/json");

                var response = await client.PostAsync("api/Players", content);

                if (response.IsSuccessStatusCode)
                {
                    Console.WriteLine($"But enough chit-chat. It's time to see what you're capable of, {playerName}. But first, close your eyes...\n");
                    Console.WriteLine("--------");
                    Console.WriteLine("\nYou awaken to a soft luminescence bathing an environment unlike anything you’ve ever seen. " +
                        "Geometric shapes hover in the air, pulsating with an otherworldly glow. " +
                        "The ground beneath your feet feels like... nothing. You are weightless.\n");
                }
                else
                {
                    Console.WriteLine("Failed to create player.");
                }
            }
        }
    }
}
