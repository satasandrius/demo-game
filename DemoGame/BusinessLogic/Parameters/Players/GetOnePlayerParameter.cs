﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Parameters.Players
{
    public class GetOnePlayerParameter
    {
        public GetOnePlayerParameter(int id)
        {
            if (id < 1)
            {
                throw new ArgumentException("No such ID found.");
            }

            Id = id;
        }

        public int Id { get; }
    }
}
