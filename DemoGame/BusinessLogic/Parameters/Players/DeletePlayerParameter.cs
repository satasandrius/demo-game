﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Parameters.Players
{
    public class DeletePlayerParameter
    {
        public DeletePlayerParameter(int id)
        {
            Id = id;
        }

        public int Id { get; }
    }
}
