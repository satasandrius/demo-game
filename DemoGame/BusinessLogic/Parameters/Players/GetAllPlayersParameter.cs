﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Parameters.Players
{
    public class GetAllPlayersParameter
    {
        public GetAllPlayersParameter(int? limit)
        {
            Limit = limit;
        }

        public int? Limit { get; }
    }
}
