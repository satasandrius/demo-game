﻿using Shared.Dto.Items;
using Shared.Dto.Players;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Parameters.Players
{
    public class CreatePlayerParameter
    {
        public CreatePlayerParameter(CreatePlayerDto player)
        {
            Player = player;
        }

        public CreatePlayerDto Player { get; }
    }
}
