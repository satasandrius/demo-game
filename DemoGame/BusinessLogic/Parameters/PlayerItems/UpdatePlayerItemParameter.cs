﻿using Shared.Dto.PlayerItems;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Parameters.PlayerItems
{
    public class UpdatePlayerItemParameter
    {
        public UpdatePlayerItemParameter(UpdatePlayerItemDto playerItem)
        {
            PlayerItem = playerItem;
        }

        public UpdatePlayerItemDto PlayerItem { get; }
    }
}
