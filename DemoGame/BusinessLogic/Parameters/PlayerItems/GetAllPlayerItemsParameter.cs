﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Parameters.PlayerItems
{
    public class GetAllPlayerItemsParameter
    {
        public GetAllPlayerItemsParameter(int? limit)
        {
            Limit = limit;
        }

        public int? Limit { get; }
    }
}
