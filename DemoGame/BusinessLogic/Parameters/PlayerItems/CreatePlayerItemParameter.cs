﻿using Shared.Dto.PlayerItems;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Parameters.PlayerItems
{
    public class CreatePlayerItemParameter
    {
        public CreatePlayerItemParameter(CreatePlayerItemDto playerItem)
        {
            PlayerItem = playerItem;
        }

        public CreatePlayerItemDto PlayerItem { get; }
    }
}
