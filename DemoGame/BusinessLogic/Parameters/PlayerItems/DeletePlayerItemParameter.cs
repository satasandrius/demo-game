﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Parameters.PlayerItems
{
    public class DeletePlayerItemParameter
    {
        public DeletePlayerItemParameter(int id)
        {
            Id = id;
        }

        public int Id { get; }
    }
}
