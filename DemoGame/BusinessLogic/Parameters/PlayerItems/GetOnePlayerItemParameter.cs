﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Parameters.PlayerItems
{
    public class GetOnePlayerItemParameter
    {
        public GetOnePlayerItemParameter(int id)
        {
            if (id < 1)
            {
                throw new ArgumentException("No such ID found.");
            }

            Id = id;
        }

        public int Id { get; }
    }
}
