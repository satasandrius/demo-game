﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Parameters.Levels
{
    public class DeleteLevelParameter
    {
        public DeleteLevelParameter(int id)
        {
            Id = id;
        }

        public int Id { get; }
    }
}
