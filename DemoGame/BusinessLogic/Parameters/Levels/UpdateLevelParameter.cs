﻿using Shared.Dto.Levels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Parameters.Levels
{
    public class UpdateLevelParameter
    {
        public UpdateLevelParameter(UpdateLevelDto level)
        {
            Level = level;
        }

        public UpdateLevelDto Level { get; }
    }
}
