﻿using Shared.Dto.Levels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Parameters.Levels
{
    public class CreateLevelParameter
    {
        public CreateLevelParameter(CreateLevelDto level)
        {
            Level = level;
        }

        public CreateLevelDto Level { get; }
    }
}
