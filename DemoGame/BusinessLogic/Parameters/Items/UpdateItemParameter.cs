﻿using Shared.Dto.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Parameters.Items
{
    public class UpdateItemParameter
    {
        public UpdateItemParameter(UpdateItemDto item)
        {
            Item = item;
        }

        public UpdateItemDto Item { get; }
    }
}
