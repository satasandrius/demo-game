﻿using Shared.Dto.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Parameters.Items
{
    public class CreateItemParameter
    {
        public CreateItemParameter(CreateItemDto item)
        {
            Item = item;
        }

        public CreateItemDto Item { get; }
    }
}
