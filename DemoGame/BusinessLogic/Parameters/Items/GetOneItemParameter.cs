﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Parameters.Items
{
    public class GetOneItemParameter
    {
        public GetOneItemParameter(int id)
        {
            if (id < 1)
            {
                throw new ArgumentException("No such ID found.");
            }

            Id = id;
        }

        public int Id { get; }
    }
}
