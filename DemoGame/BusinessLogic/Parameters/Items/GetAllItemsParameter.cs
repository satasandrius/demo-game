﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Parameters.Items
{
    public class GetAllItemsParameter
    {
        public GetAllItemsParameter(int? limit)
        {
            Limit = limit;
        }

        public int? Limit { get; }
    }
}
