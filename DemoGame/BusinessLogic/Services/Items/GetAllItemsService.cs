﻿using BusinessLogic.Parameters.Items;
using DataAccess.Entities;
using DataAccess.Repositories;
using Shared.Dto.Items;

namespace BusinessLogic.Services.Items
{
    public class GetAllItemsService : IService<GetAllItemsParameter, List<ItemDto>>
    {
        private readonly IRepository<Item> _itemRepository;

        public GetAllItemsService(IRepository<Item> itemRepository)
        {
            _itemRepository = itemRepository;
        }

        public async Task<List<ItemDto>> CallAsync(GetAllItemsParameter parameter)
        {
            if (parameter == null)
            {
                throw new ArgumentNullException(nameof(parameter));
            }

            var result = await _itemRepository.GetAllAsync();

            return result.Select(Convert).ToList();
        }

        private ItemDto Convert(Item item)
        {
            return new ItemDto
            {
                Id = item.Id,
                Name = item.Name,
                Description = item.Description,
                IsFound = item.IsFound,
                IsUsed = item.IsUsed
            };
        }
    }
}
