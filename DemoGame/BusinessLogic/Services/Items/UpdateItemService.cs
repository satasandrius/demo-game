﻿using BusinessLogic.Parameters.Items;
using DataAccess.Entities;
using DataAccess.Repositories;
using Shared.Dto.Items;

namespace BusinessLogic.Services.Items
{
    public class UpdateItemService : IService<UpdateItemParameter, ItemDto>
    {
        private readonly IRepository<Item> _itemRepository;

        public UpdateItemService(IRepository<Item> itemRepository)
        {
            _itemRepository = itemRepository;
        }

        public async Task<ItemDto> CallAsync(UpdateItemParameter parameter)
        {
            if (parameter == null)
            {
                throw new ArgumentNullException(nameof(parameter));
            }

            var itemEntity = Convert(parameter.Item);

            await _itemRepository.UpdateAsync(itemEntity);

            if (itemEntity == null)
            {
                throw new ArgumentNullException(nameof(itemEntity));
            }

            return Convert(itemEntity);
        }

        private Item Convert(UpdateItemDto itemDto)
        {
            return new Item(
                itemDto.Id,
                itemDto.Name,
                itemDto.Description,
                itemDto.IsFound,
                itemDto.IsUsed);
        }

        private ItemDto Convert(Item item)
        {
            return new ItemDto
            {
                Id = item.Id,
                Name = item.Name,
                Description = item.Description,
                IsFound = item.IsFound,
                IsUsed = item.IsUsed
            };
        }
    }
}
