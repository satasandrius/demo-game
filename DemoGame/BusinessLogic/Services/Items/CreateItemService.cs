﻿using BusinessLogic.Parameters.Items;
using DataAccess.Entities;
using DataAccess.Repositories;
using Shared.Dto.Items;

namespace BusinessLogic.Services.Items
{
    public class CreateItemService : IService<CreateItemParameter, ItemDto>
    {
        private readonly IRepository<Item> _itemRepository;

        public CreateItemService(IRepository<Item> itemRepository)
        {
            _itemRepository = itemRepository;
        }

        public async Task<ItemDto> CallAsync(CreateItemParameter parameter)
        {
            if (parameter == null)
            {
                throw new ArgumentNullException(nameof(parameter));
            }

            var itemEntity = Convert(parameter.Item);

            var result = await _itemRepository.CreateAsync(itemEntity);

            return Convert(result);
        }

        private Item Convert(CreateItemDto itemDto)
        {
            return new Item(
                0,
                itemDto.Name,
                itemDto.Description,
                false,
                false);
        }

        private ItemDto Convert(Item item)
        {
            return new ItemDto
            {
                Id = item.Id,
                Name = item.Name,
                Description = item.Description,
                IsFound = item.IsFound,
                IsUsed = item.IsUsed
            };
        }
    }
}
