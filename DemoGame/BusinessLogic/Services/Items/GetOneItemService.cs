﻿using BusinessLogic.Parameters.Items;
using DataAccess.Entities;
using DataAccess.Repositories;
using Shared.Dto.Items;

namespace BusinessLogic.Services.Items
{
    public class GetOneItemService : IService<GetOneItemParameter, ItemDto>
    {
        private readonly IRepository<Item> _itemRepository;

        public GetOneItemService(IRepository<Item> itemRepository)
        {
            _itemRepository = itemRepository;
        }

        public async Task<ItemDto> CallAsync(GetOneItemParameter parameter)
        {
            if (parameter == null)
            {
                throw new ArgumentNullException(nameof(parameter));
            }

            var result = await _itemRepository.GetByIdAsync(parameter.Id);

            if (result == null)
            {
                throw new InvalidOperationException($"Item with ID {parameter.Id} was not found.");
            }

            return Convert(result);
        }

        private ItemDto Convert(Item item)
        {
            return new ItemDto
            {
                Id = item.Id,
                Name = item.Name,
                Description = item.Description,
                IsFound = item.IsFound,
                IsUsed = item.IsUsed
            };
        }
    }
}
