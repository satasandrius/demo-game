﻿using BusinessLogic.Parameters.Items;
using DataAccess.Entities;
using DataAccess.Repositories;
using Shared.Dto.Items;

namespace BusinessLogic.Services.Items
{
    public class DeleteItemService : IService<DeleteItemParameter, ItemDto>
    {
        private readonly IRepository<Item> _itemRepository;

        public DeleteItemService(IRepository<Item> itemRepository)
        {
            _itemRepository = itemRepository;
        }

        public async Task<ItemDto> CallAsync(DeleteItemParameter parameter)
        {
            if (parameter == null)
            {
                throw new ArgumentNullException(nameof(parameter));
            }

            var result = await _itemRepository.DeleteAsync(parameter.Id);

            if (result)
            {
                return new ItemDto
                {
                    Id = parameter.Id
                };
            }
            else
            {
                throw new KeyNotFoundException($"Item with ID {parameter.Id} was not found.");
            }
        }
    }
}
