﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Services
{
    public interface IService<TParameter, TData>
    {
        Task<TData> CallAsync(TParameter parameter);
    }
}
