﻿using BusinessLogic.Parameters.Levels;
using DataAccess.Entities;
using DataAccess.Repositories;
using Shared.Dto.Levels;

namespace BusinessLogic.Services.Levels
{
    public class GetOneLevelService : IService<GetOneLevelParameter, LevelDto>
    {
        private readonly IRepository<Level> _levelRepository;

        public GetOneLevelService(IRepository<Level> levelRepository)
        {
            _levelRepository = levelRepository;
        }

        public async Task<LevelDto> CallAsync(GetOneLevelParameter parameter)
        {
            if (parameter == null)
            {
                throw new ArgumentNullException(nameof(parameter));
            }

            var result = await _levelRepository.GetByIdAsync(parameter.Id);

            if (result == null)
            {
                throw new InvalidOperationException($"Level with ID {parameter.Id} was not found.");
            }

            return Convert(result);
        }

        private LevelDto Convert(Level level)
        {
            return new LevelDto
            {
                Id = level.Id,
                Name = level.Name,
                Description = level.Description,
                Riddle = level.Riddle,
                Solution = level.Solution,
                IsSolved = level.IsSolved,
                ItemId = (int)level.ItemId
            };
        }
    }
}
