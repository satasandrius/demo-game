﻿using BusinessLogic.Parameters.Levels;
using DataAccess.Entities;
using DataAccess.Repositories;
using Shared.Dto.Levels;

namespace BusinessLogic.Services.Levels
{
    public class GetAllLevelsService : IService<GetAllLevelsParameter, List<LevelDto>>
    {
        private readonly IRepository<Level> _levelRepository;

        public GetAllLevelsService(IRepository<Level> levelRepository)
        {
            _levelRepository = levelRepository;
        }

        public async Task<List<LevelDto>> CallAsync(GetAllLevelsParameter parameter)
        {
            if (parameter == null)
            {
                throw new ArgumentNullException(nameof(parameter));
            }

            var result = await _levelRepository.GetAllAsync();

            return result.Select(Convert).ToList();
        }

        private LevelDto Convert(Level level)
        {
            return new LevelDto
            {
                Id = level.Id,
                Name = level.Name,
                Description = level.Description,
                Riddle = level.Riddle,
                Solution = level.Solution,
                IsSolved = level.IsSolved,
                ItemId = (int)level.ItemId
            };
        }
    }
}
