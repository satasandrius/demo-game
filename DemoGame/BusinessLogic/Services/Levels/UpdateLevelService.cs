﻿using BusinessLogic.Parameters.Levels;
using DataAccess.Entities;
using DataAccess.Repositories;
using Shared.Dto.Levels;

namespace BusinessLogic.Services.Levels
{
    public class UpdateLevelService : IService<UpdateLevelParameter, LevelDto>
    {
        private readonly IRepository<Level> _levelRepository;

        public UpdateLevelService(IRepository<Level> levelRepository)
        {
            _levelRepository = levelRepository;
        }

        public async Task<LevelDto> CallAsync(UpdateLevelParameter parameter)
        {
            if (parameter == null)
            {
                throw new ArgumentNullException(nameof(parameter));
            }

            var levelEntity = Convert(parameter.Level);

            await _levelRepository.UpdateAsync(levelEntity);

            if (levelEntity == null)
            {
                throw new ArgumentNullException(nameof(levelEntity));
            }

            return Convert(levelEntity);
        }

        private Level Convert(UpdateLevelDto levelDto)
        {
            return new Level(
                levelDto.Id,
                levelDto.Name,
                levelDto.Description,
                levelDto.Riddle,
                levelDto.Solution,
                levelDto.IsSolved,
                levelDto.ItemId);
        }

        private LevelDto Convert(Level level)
        {
            return new LevelDto
            {
                Id = level.Id,
                Name = level.Name,
                Description = level.Description,
                Riddle = level.Riddle,
                Solution = level.Solution,
                IsSolved = level.IsSolved,
                ItemId = (int)level.ItemId
            };
        }
    }
}
