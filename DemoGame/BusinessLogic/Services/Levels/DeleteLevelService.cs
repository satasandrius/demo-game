﻿using BusinessLogic.Parameters.Levels;
using DataAccess.Entities;
using DataAccess.Repositories;
using Shared.Dto.Levels;

namespace BusinessLogic.Services.Levels
{
    public class DeleteLevelService : IService<DeleteLevelParameter, LevelDto>
    {
        private readonly IRepository<Level> _levelRepository;

        public DeleteLevelService(IRepository<Level> levelRepository)
        {
            _levelRepository = levelRepository;
        }

        public async Task<LevelDto> CallAsync(DeleteLevelParameter parameter)
        {
            if (parameter == null)
            {
                throw new ArgumentNullException(nameof(parameter));
            }

            var result = await _levelRepository.DeleteAsync(parameter.Id);

            if (result)
            {
                return new LevelDto
                {
                    Id = parameter.Id
                };
            }
            else
            {
                throw new KeyNotFoundException($"Level with ID {parameter.Id} was not found.");
            }
        }
    }
}
