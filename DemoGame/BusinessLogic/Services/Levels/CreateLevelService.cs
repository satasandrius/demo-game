﻿using BusinessLogic.Parameters.Levels;
using DataAccess.Entities;
using DataAccess.Repositories;
using Shared.Dto.Levels;

namespace BusinessLogic.Services.Items
{
    public class CreateLevelService : IService<CreateLevelParameter, LevelDto>
    {
        private readonly IRepository<Level> _levelRepository;

        public CreateLevelService(IRepository<Level> levelRepository)
        {
            _levelRepository = levelRepository;
        }

        public async Task<LevelDto> CallAsync(CreateLevelParameter parameter)
        {
            if (parameter == null)
            {
                throw new ArgumentNullException(nameof(parameter));
            }

            var levelEntity = Convert(parameter.Level);

            var result = await _levelRepository.CreateAsync(levelEntity);

            return Convert(result);
        }

        private Level Convert(CreateLevelDto levelDto)
        {
            return new Level(
                0,
                levelDto.Name,
                levelDto.Description,
                levelDto.Riddle,
                levelDto.Solution,
                false,
                levelDto.ItemId);
        }

        private LevelDto Convert(Level level)
        {
            return new LevelDto
            {
                Id = level.Id,
                Name = level.Name,
                Description = level.Description,
                Riddle = level.Riddle,
                Solution = level.Solution,
                IsSolved = false,
                ItemId = (int)level.ItemId
            };
        }
    }
}
