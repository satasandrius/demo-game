﻿using DataAccess.Entities;
using DataAccess.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Services.Decorators
{
    public class LoggingPlayerRepositoryDecorator : IRepository<Player>
    {
        private readonly IRepository<Player> _repository;
        private readonly string _logFilePath;

        public LoggingPlayerRepositoryDecorator(IRepository<Player> repository, string logFilePath)
        {
            _repository = repository;
            _logFilePath = logFilePath;
        }

        public async Task<Player> CreateAsync(Player entity)
        {
            string logMessage = $"{DateTime.Now} Creating player with name {entity.Name}...";
            File.AppendAllText(_logFilePath, logMessage + Environment.NewLine);

            var result = await _repository.CreateAsync(entity);

            logMessage = $"Created player with ID {result.Id}";
            File.AppendAllText(_logFilePath, logMessage + Environment.NewLine);

            return result;
        }

        public Task<bool> DeleteAsync(int id)
        {
            throw new NotImplementedException();
        }

        public Task<List<Player>> GetAllAsync(params Expression<Func<Player, object>>[] includes)
        {
            throw new NotImplementedException();
        }

        public Task<Player> GetByIdAsync(int id)
        {
            throw new NotImplementedException();
        }

        public Task UpdateAsync(Player entity)
        {
            throw new NotImplementedException();
        }
    }
}
