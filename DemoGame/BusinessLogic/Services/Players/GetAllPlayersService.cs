﻿using BusinessLogic.Parameters.Players;
using DataAccess.Entities;
using DataAccess.Repositories;
using Shared.Dto.Players;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Services.Players
{
    public class GetAllPlayersService : IService<GetAllPlayersParameter, List<PlayerDto>>
    {
        private readonly IRepository<Player> _playerRepository;

        public GetAllPlayersService(IRepository<Player> playerRepository)
        {
            _playerRepository = playerRepository;
        }

        public async Task<List<PlayerDto>> CallAsync(GetAllPlayersParameter parameter)
        {
            if (parameter == null)
            {
                throw new ArgumentNullException(nameof(parameter));
            }

            var result = await _playerRepository.GetAllAsync();

            return result.Select(Convert).ToList();
        }

        private PlayerDto Convert(Player player)
        {
            return new PlayerDto
            {
                Id = player.Id,
                Name = player.Name,
                Lucky = player.Lucky
            };
        }
    }
}
