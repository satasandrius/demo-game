﻿using BusinessLogic.Parameters.Players;
using DataAccess.Entities;
using DataAccess.Repositories;
using Shared.Dto.Players;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Services.Players
{
    public class GetOnePlayerService : IService<GetOnePlayerParameter, PlayerDto>
    {
        private readonly IRepository<Player> _playerRepository;

        public GetOnePlayerService(IRepository<Player> playerRepository)
        {
            _playerRepository = playerRepository;
        }

        public async Task<PlayerDto> CallAsync(GetOnePlayerParameter parameter)
        {
            if (parameter == null)
            {
                throw new ArgumentNullException(nameof(parameter));
            }

            var result = await _playerRepository.GetByIdAsync(parameter.Id);

            if (result == null)
            {
                throw new InvalidOperationException($"Player with ID {parameter.Id} was not found.");
            }

            return Convert(result);
        }

        private PlayerDto Convert(Player player)
        {
            return new PlayerDto
            {
                Id = player.Id,
                Name = player.Name,
                Lucky = player.Lucky
            };
        }
    }
}
