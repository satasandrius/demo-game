﻿using BusinessLogic.Parameters.Players;
using DataAccess.Entities;
using DataAccess.Repositories;
using Shared.Dto.Players;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Services.Players
{
    public class UpdatePlayerService : IService<UpdatePlayerParameter, PlayerDto>
    {
        private readonly IRepository<Player> _playerRepository;

        public UpdatePlayerService(IRepository<Player> playerRepository)
        {
            _playerRepository = playerRepository;
        }

        public async Task<PlayerDto> CallAsync(UpdatePlayerParameter parameter)
        {
            if (parameter == null)
            {
                throw new ArgumentNullException(nameof(parameter));
            }

            var playerEntity = Convert(parameter.Player);

            await _playerRepository.UpdateAsync(playerEntity);

            if (playerEntity == null)
            {
                throw new ArgumentNullException(nameof(playerEntity));
            }

            return Convert(playerEntity);
        }

        private Player Convert(UpdatePlayerDto playerDto)
        {
            return new Player(
                playerDto.Id,
                playerDto.Name,
                playerDto.Lucky,
                playerDto.Points);
        }

        private PlayerDto Convert(Player player)
        {
            return new PlayerDto
            {
                Id = player.Id,
                Name = player.Name,
                Lucky = player.Lucky,
                Points = player.Points
            };
        }
    }
}
