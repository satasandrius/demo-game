﻿using BusinessLogic.Parameters.Players;
using BusinessLogic.Services.Decorators;
using DataAccess.Entities;
using DataAccess.Repositories;
using Shared.Dto.Players;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Services.Players
{
    public class CreatePlayerService : IService<CreatePlayerParameter, PlayerDto>
    {
        private readonly IRepository<Player> _playerRepository;

        public CreatePlayerService(IRepository<Player> playerRepository)
        {
            _playerRepository = playerRepository;
            _playerRepository = new LoggingPlayerRepositoryDecorator(playerRepository, "CreatedPlayersLog.txt");
        }

        public async Task<PlayerDto> CallAsync(CreatePlayerParameter parameter)
        {
            if (parameter == null)
            {
                throw new ArgumentNullException(nameof(parameter));
            }

            var playerEntity = Convert(parameter.Player);

            var result = await _playerRepository.CreateAsync(playerEntity);

            return Convert(result);
        }

        private Player Convert(CreatePlayerDto playerDto)
        {
            return new Player(
                0,
                playerDto.Name,
                playerDto.Lucky,
                playerDto.Points);
        }

        private PlayerDto Convert(Player player)
        {
            return new PlayerDto
            {
                Id = player.Id,
                Name = player.Name,
                Lucky = player.Lucky,
                Points = player.Points
            };
        }
    }
}
