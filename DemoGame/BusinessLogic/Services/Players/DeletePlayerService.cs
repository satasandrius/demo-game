﻿using BusinessLogic.Parameters.Players;
using DataAccess.Entities;
using DataAccess.Repositories;
using Shared.Dto.Players;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Services.Players
{
    public class DeletePlayerService : IService<DeletePlayerParameter, PlayerDto>
    {
        private readonly IRepository<Player> _playerRepository;

        public DeletePlayerService(IRepository<Player> playerRepository)
        {
            _playerRepository = playerRepository;
        }

        public async Task<PlayerDto> CallAsync(DeletePlayerParameter parameter)
        {
            if (parameter == null)
            {
                throw new ArgumentNullException(nameof(parameter));
            }

            var result = await _playerRepository.DeleteAsync(parameter.Id);

            if (result)
            {
                return new PlayerDto
                {
                    Id = parameter.Id
                };
            }
            else
            {
                throw new KeyNotFoundException($"Player with ID {parameter.Id} was not found.");
            }
        }
    }
}
