﻿using BusinessLogic.Parameters.PlayerItems;
using DataAccess.Entities;
using DataAccess.Repositories;
using Shared.Dto.PlayerItems;

namespace BusinessLogic.Services.PlayerItems
{
    public class GetOnePlayerItemService : IService<GetOnePlayerItemParameter, PlayerItemDto>
    {
        private readonly IRepository<PlayerItem> _playerItemRepository;

        public GetOnePlayerItemService(IRepository<PlayerItem> playerItemRepository)
        {
            _playerItemRepository = playerItemRepository;
        }

        public async Task<PlayerItemDto> CallAsync(GetOnePlayerItemParameter parameter)
        {
            if (parameter == null)
            {
                throw new ArgumentNullException(nameof(parameter));
            }

            var result = await _playerItemRepository.GetByIdAsync(parameter.Id);

            if (result == null)
            {
                throw new InvalidOperationException($"PlayerItem with ID {parameter.Id} was not found.");
            }

            return Convert(result);
        }

        private PlayerItemDto Convert(PlayerItem playerItem)
        {
            return new PlayerItemDto
            {
                Id = playerItem.Id,
                PlayerId = (int)playerItem.PlayerId,
                ItemId = (int)playerItem.ItemId
            };
        }
    }
}
