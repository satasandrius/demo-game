﻿using BusinessLogic.Parameters.PlayerItems;
using DataAccess;
using DataAccess.Entities;
using DataAccess.Repositories;
using Microsoft.EntityFrameworkCore;
using Shared.Dto.Items;
using Shared.Dto.PlayerItems;
using Shared.Dto.Players;

namespace BusinessLogic.Services.PlayerItems
{
    public class GetAllPlayerItemsService : IService<GetAllPlayerItemsParameter, List<PlayerItemDto>>
    {
        private readonly IRepository<PlayerItem> _playerItemRepository;

        public GetAllPlayerItemsService(
            IRepository<PlayerItem> playerItemRepository)
        {
            _playerItemRepository = playerItemRepository;
        }

        public async Task<List<PlayerItemDto>> CallAsync(GetAllPlayerItemsParameter parameter)
        {
            if (parameter == null)
            {
                throw new ArgumentNullException(nameof(parameter));
            }

            var result = await _playerItemRepository.GetAllAsync(x => x.Player, x => x.Item);
            var convertedItems = result.Select(x => Convert(x)).ToList();

            return convertedItems;
        }

        private PlayerItemDto Convert(PlayerItem playerItem)
        {
            return new PlayerItemDto
            {
                Id = playerItem.Id,
                PlayerId = playerItem.PlayerId,
                ItemId = playerItem.ItemId,
                Player = new PlayerDto
                {
                    Id = playerItem.Player.Id,
                    Name = playerItem.Player.Name,
                    Lucky = playerItem.Player.Lucky
                },
                Items = new List<ItemDto>
                    {
                        new ItemDto
                        {
                            Id = playerItem.Item.Id,
                            Name = playerItem.Item.Name,
                            Description = playerItem.Item.Description,
                            IsFound = playerItem.Item.IsFound,
                            IsUsed = playerItem.Item.IsUsed
                        }
                    }
            };
        }
    }
}
