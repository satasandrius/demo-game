﻿using BusinessLogic.Parameters.PlayerItems;
using DataAccess.Entities;
using DataAccess.Repositories;
using Shared.Dto.PlayerItems;

namespace BusinessLogic.Services.PlayerItems
{
    public class UpdatePlayerItemService : IService<UpdatePlayerItemParameter, PlayerItemDto>
    {
        private readonly IRepository<PlayerItem> _playerItemRepository;

        public UpdatePlayerItemService(IRepository<PlayerItem> playerItemRepository)
        {
            _playerItemRepository = playerItemRepository;
        }

        public async Task<PlayerItemDto> CallAsync(UpdatePlayerItemParameter parameter)
        {
            if (parameter == null)
            {
                throw new ArgumentNullException(nameof(parameter));
            }

            var playerItemEntity = Convert(parameter.PlayerItem);

            await _playerItemRepository.UpdateAsync(playerItemEntity);

            if (playerItemEntity == null)
            {
                throw new ArgumentNullException(nameof(playerItemEntity));
            }

            return Convert(playerItemEntity);
        }

        private PlayerItem Convert(UpdatePlayerItemDto playerItemDto)
        {
            return new PlayerItem(
                playerItemDto.Id,
                playerItemDto.PlayerId,
                playerItemDto.ItemId
            );
        }

        private PlayerItemDto Convert(PlayerItem playerItem)
        {
            return new PlayerItemDto
            {
                Id = playerItem.Id,
                PlayerId = (int)playerItem.PlayerId,
                ItemId = (int)playerItem.ItemId
            };
        }
    }
}
