﻿using BusinessLogic.Parameters.PlayerItems;
using DataAccess.Entities;
using DataAccess.Repositories;
using Shared.Dto.PlayerItems;

namespace BusinessLogic.Services.PlayerItems
{
    public class DeletePlayerItemService : IService<DeletePlayerItemParameter, PlayerItemDto>
    {
        private readonly IRepository<PlayerItem> _playerItemRepository;

        public DeletePlayerItemService(IRepository<PlayerItem> playerItemRepository)
        {
            _playerItemRepository = playerItemRepository;
        }

        public async Task<PlayerItemDto> CallAsync(DeletePlayerItemParameter parameter)
        {
            if (parameter == null)
            {
                throw new ArgumentNullException(nameof(parameter));
            }

            var result = await _playerItemRepository.DeleteAsync(parameter.Id);

            if (result)
            {
                return new PlayerItemDto
                {
                    Id = parameter.Id
                };
            }
            else
            {
                throw new KeyNotFoundException($"PlayerItem with ID {parameter.Id} was not found.");
            }
        }
    }
}
