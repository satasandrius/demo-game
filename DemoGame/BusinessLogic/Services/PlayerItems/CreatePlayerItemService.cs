﻿using BusinessLogic.Parameters.PlayerItems;
using DataAccess.Entities;
using DataAccess.Repositories;
using Shared.Dto.Items;
using Shared.Dto.PlayerItems;

namespace BusinessLogic.Services.PlayerItems
{
    public class CreatePlayerItemService : IService<CreatePlayerItemParameter, PlayerItemDto>
    {
        private readonly IRepository<PlayerItem> _playerItemRepository;

        public CreatePlayerItemService(IRepository<PlayerItem> playerItemRepository)
        {
            _playerItemRepository = playerItemRepository;
        }

        public async Task<PlayerItemDto> CallAsync(CreatePlayerItemParameter parameter)
        {
            if (parameter == null)
            {
                throw new ArgumentNullException(nameof(parameter));
            }

            var playerItemEntity = Convert(parameter.PlayerItem);

            var result = await _playerItemRepository.CreateAsync(playerItemEntity);

            return Convert(result);
        }

        private PlayerItem Convert(CreatePlayerItemDto playerItemDto)
        {
            return new PlayerItem(
                0,
                playerItemDto.PlayerId,
                playerItemDto.ItemId);
        }

        private PlayerItemDto Convert(PlayerItem playerItem)
        {
            return new PlayerItemDto
            {
                Id = playerItem.Id,
                PlayerId = (int)playerItem.PlayerId,
                ItemId = (int)playerItem.ItemId
            };
        }
    }
}
