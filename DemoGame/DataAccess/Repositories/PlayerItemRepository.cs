﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess.Entities;

namespace DataAccess.Repositories
{
    public class PlayerItemRepository : BaseRepository<PlayerItem>
    {
        public PlayerItemRepository(GameDbContext context) : base(context)
        {
        }
    }
}
