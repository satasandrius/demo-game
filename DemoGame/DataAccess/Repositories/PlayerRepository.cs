﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess.Entities;

namespace DataAccess.Repositories
{
    public class PlayerRepository : BaseRepository<Player>
    {
        public PlayerRepository(GameDbContext context) : base(context)
        {
        }
    }
}
