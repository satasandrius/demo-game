﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using DataAccess.Entities;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Repositories
{
    public class BaseRepository<TEntity> : IRepository<TEntity> where TEntity : BaseEntity
    {
        private GameDbContext _context;
        private DbSet<TEntity> _entities;

        public BaseRepository(GameDbContext context)
        {
            _context = context;
            _entities = _context.Set<TEntity>();
        }

        public async Task<TEntity> CreateAsync(TEntity entity)
        {
            await _entities.AddAsync(entity);
            await _context.SaveChangesAsync();

            return entity;
        }

        public async Task<bool> DeleteAsync(int id)
        {
            TEntity entity = await GetByIdAsync(id);

            if (entity == null)
            {
                return false;
            }

            _entities.Remove(entity);
            await _context.SaveChangesAsync();
            return true;
        }

        public async Task<List<TEntity>> GetAllAsync(params Expression<Func<TEntity, object>>[] includes)
        {
            IQueryable<TEntity> query = _entities;

            foreach (var include in includes)
            {
                query = query.Include(include);
            }

            return await query.ToListAsync();
        }

        public async Task<TEntity> GetByIdAsync(int id)
        {
            IQueryable<TEntity> query = _entities;

            if (query == null)
            {
                throw new KeyNotFoundException($"{typeof(TEntity).Name} with ID {id} is not found.");
            }

            return await query.FirstOrDefaultAsync(query => query.Id == id);
        }

        public async Task UpdateAsync(TEntity entity)
        {
            var existingEntity = await _entities.FindAsync(entity.Id);

            if (existingEntity == null)
            {
                throw new KeyNotFoundException($"{typeof(TEntity).Name} with ID {entity.Id} is not found.");
            }

            _context.Entry(existingEntity).CurrentValues.SetValues(entity);

            await _context.SaveChangesAsync();
        }
    }
}
