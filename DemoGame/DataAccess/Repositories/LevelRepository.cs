﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess.Entities;

namespace DataAccess.Repositories
{
    public class LevelRepository : BaseRepository<Level>
    {
        public LevelRepository(GameDbContext context) : base(context)
        {
        }
    }
}
