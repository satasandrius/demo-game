﻿using DataAccess.Entities;
using Microsoft.EntityFrameworkCore;

namespace DataAccess
{
    public class GameDbContext : DbContext
    {
        public GameDbContext()
        {
        }

        public GameDbContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Player> Players { get; set; }
        public DbSet<Item> Items { get; set; }
        public DbSet<Level> Levels { get; set; }
        public DbSet<PlayerItem> PlayerItems { get; set; }
    }
}
