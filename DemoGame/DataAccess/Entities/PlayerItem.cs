﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Entities
{
    public class PlayerItem : BaseEntity
    {
        public PlayerItem(
            int id,
            int? playerId,
            int? itemId)
            : base(id)
        {
            PlayerId = playerId;
            ItemId = itemId;
        }

        public int? PlayerId { get; private set; }
        public Player? Player { get; set; }

        public int? ItemId { get; private set; }
        public Item? Item { get; set; }
    }
}
