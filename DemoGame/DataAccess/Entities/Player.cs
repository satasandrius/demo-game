﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Entities
{
    public class Player : BaseEntity
    {
        public Player(
            int id,
            string name,
            bool lucky,
            int points)
            : base(id)
        {
            Name = name;
            Lucky = lucky;
            Points = points;
        }

        [Required]
        [MaxLength(50)]
        public string Name { get; private set; }
        [Required]
        public bool Lucky { get; private set; }
        public int Points { get; private set; }
    }
}
