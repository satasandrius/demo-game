﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Entities
{
    public class BaseEntity
    {
        protected BaseEntity()
        {
        }

        public BaseEntity(int id)
        {
            if (id != 0)
            {
                Id = id;
            }
        }

        public int Id { get; private set; }
    }
}
