﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Entities
{
    public class Item : BaseEntity
    {
        public Item(
            int id,
            string name,
            string description,
            bool isFound,
            bool isUsed)
            : base(id)
        {
            Name = name;
            Description = description;
            IsFound = isFound;
            IsUsed = isUsed;
        }

        [Required]
        [MaxLength(50)]
        public string Name { get; private set; }
        [Required]
        [MaxLength(200)]
        public string Description { get; private set; }
        public bool IsFound { get; private set; }
        public bool IsUsed { get; private set; }
    }
}
