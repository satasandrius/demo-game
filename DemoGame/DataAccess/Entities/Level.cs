﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataAccess.Entities
{
    public class Level : BaseEntity
    {
        public Level(
            int id,
            string name,
            string description,
            string riddle,
            string solution,
            bool isSolved,
            int? itemId)
            : base(id)
        {
            Name = name;
            Description = description;
            Riddle = riddle;
            Solution = solution;
            IsSolved = isSolved;
            ItemId = itemId;
        }

        [Required]
        [MaxLength(50)]
        public string Name { get; private set; }
        [Required]
        [MaxLength(300)]
        public string Description { get; private set; }
        [Required]
        [MaxLength(300)]
        public string Riddle { get; private set; }
        [Required]
        [MaxLength(300)]
        public string Solution { get; private set; }
        [Required]
        public bool IsSolved { get; private set; }
        public int? ItemId { get; set; }
        public Item? Item { get; set; }
    }
}
