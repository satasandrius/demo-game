﻿using BusinessLogic.Parameters.Players;
using BusinessLogic.Services;
using GameApi.Controllers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Shared.Dto.Players;

namespace GameApi.Tests
{
    [TestClass]
    public class PlayersControllerTests
    {
        private Mock<IService<CreatePlayerParameter, PlayerDto>> _createPlayerServiceMock;
        private Mock<IService<GetOnePlayerParameter, PlayerDto>> _getOnePlayerServiceMock;
        private Mock<IService<GetAllPlayersParameter, List<PlayerDto>>> _getAllPlayersServiceMock;
        private Mock<IService<UpdatePlayerParameter, PlayerDto>> _updatePlayerServiceMock;
        private Mock<IService<DeletePlayerParameter, PlayerDto>> _deletePlayerServiceMock;
        private PlayersController _controller;

        [TestInitialize]
        public void Initialize()
        {
            _createPlayerServiceMock = new Mock<IService<CreatePlayerParameter, PlayerDto>>();
            _getOnePlayerServiceMock = new Mock<IService<GetOnePlayerParameter, PlayerDto>>();
            _getAllPlayersServiceMock = new Mock<IService<GetAllPlayersParameter, List<PlayerDto>>>();
            _updatePlayerServiceMock = new Mock<IService<UpdatePlayerParameter, PlayerDto>>();
            _deletePlayerServiceMock = new Mock<IService<DeletePlayerParameter, PlayerDto>>();

            _controller = new PlayersController(
                _createPlayerServiceMock.Object,
                _getAllPlayersServiceMock.Object,
                _getOnePlayerServiceMock.Object,
                _updatePlayerServiceMock.Object,
                _deletePlayerServiceMock.Object);
        }

        [TestMethod]
        public async Task CreateAsync_ShouldReturnCreatedPlayer()
        {
            // Arrange
            var expectedPlayer = new PlayerDto { Id = 1, Name = "Test Player" };
            _createPlayerServiceMock
                .Setup(service => service.CallAsync(It.IsAny<CreatePlayerParameter>()))
                .ReturnsAsync(expectedPlayer);

            var createPlayerDto = new CreatePlayerDto { Name = "Test Player" };

            // Act
            var result = await _controller.CreateAsync(createPlayerDto);

            // Assert
            var actionResult = result.Result as ObjectResult;
            Assert.IsNotNull(actionResult);
            Assert.AreEqual(StatusCodes.Status201Created, actionResult.StatusCode);

            var returnedPlayer = actionResult.Value as PlayerDto;
            Assert.IsNotNull(returnedPlayer);
            Assert.AreEqual(expectedPlayer.Id, returnedPlayer.Id);
            Assert.AreEqual(expectedPlayer.Name, returnedPlayer.Name);
        }

        [TestMethod]
        public async Task GetAllAsync_ShouldReturnAllPlayers()
        {
            // Arrange
            var expectedPlayers = new List<PlayerDto>
            {
                new PlayerDto { Id = 1, Name = "Test Player 1" },
                new PlayerDto { Id = 2, Name = "Test Player 2" }
            };
            _getAllPlayersServiceMock
                .Setup(service => service.CallAsync(It.IsAny<GetAllPlayersParameter>()))
                .ReturnsAsync(expectedPlayers);

            // Act
            var result = await _controller.GetAllAsync();

            // Assert
            var actionResult = result.Result as ObjectResult;
            Assert.IsNotNull(actionResult);
            Assert.AreEqual(StatusCodes.Status200OK, actionResult.StatusCode);

            var returnedPlayers = actionResult.Value as List<PlayerDto>;
            Assert.IsNotNull(returnedPlayers);
            Assert.AreEqual(expectedPlayers.Count, returnedPlayers.Count);

            for (int i = 0; i < expectedPlayers.Count; i++)
            {
                Assert.AreEqual(expectedPlayers[i].Id, returnedPlayers[i].Id);
                Assert.AreEqual(expectedPlayers[i].Name, returnedPlayers[i].Name);
            }
        }
    }
}