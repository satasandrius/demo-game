﻿using Shared.Dto.Items;
using Shared.Dto.Players;
using System.Text.Json.Serialization;

namespace Shared.Dto.PlayerItems
{
    public class PlayerItemDto
    {
        [JsonPropertyName("id")]
        public int Id { get; set; }
        [JsonPropertyName("playerId")]
        public int? PlayerId { get; set; }
        [JsonPropertyName("itemId")]
        public int? ItemId { get; set; }
        [JsonPropertyName("player")]
        public PlayerDto? Player { get; set; }
        [JsonPropertyName("items")]
        public List<ItemDto>? Items { get; set; }
    }
}
