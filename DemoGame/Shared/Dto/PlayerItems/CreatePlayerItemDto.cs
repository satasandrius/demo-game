﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using DataAccess.Entities;
using Shared.Dto.Items;
using Shared.Dto.Players;

namespace Shared.Dto.PlayerItems
{
    public class CreatePlayerItemDto
    {
        [JsonPropertyName("playerId")]
        public int PlayerId { get; set; }
        [JsonPropertyName("itemId")]
        public int ItemId { get; set; }

        //public List<int>? ItemsId { get; set; }
    }
}
