﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess.Entities;
using Shared.Dto.Items;
using System.Text.Json.Serialization;

namespace Shared.Dto.Levels
{
    public class LevelDto
    {
        [JsonPropertyName("id")]
        public int Id { get; set; }
        [JsonPropertyName("name")]
        public string Name { get; set; }
        [JsonPropertyName("description")]
        public string Description { get; set; }
        [JsonPropertyName("riddle")]
        public string Riddle { get; set; }
        [JsonPropertyName("solution")]
        public string Solution { get; set; }
        [JsonPropertyName("isSolved")]
        public bool IsSolved { get; set; }
        [JsonPropertyName("itemId")]
        public int ItemId { get; set; }
        //public ItemDto? Item { get; set; }
    }
}
