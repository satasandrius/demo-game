﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using Shared.Dto.Items;

namespace Shared.Dto.Levels
{
    public class UpdateLevelDto
    {
        [JsonPropertyName("id")]
        public int Id { get; set; }
        [JsonPropertyName("name")]
        public string Name { get; set; }
        [JsonPropertyName("description")]
        public string Description { get; set; }
        [JsonPropertyName("riddle")]
        public string Riddle { get; set; }
        [JsonPropertyName("solution")]
        public string Solution { get; set; }
        [JsonPropertyName("isSolved")]
        public bool IsSolved { get; set; }
        [JsonPropertyName("itemId")]
        public int ItemId { get; set; }
    }
}
