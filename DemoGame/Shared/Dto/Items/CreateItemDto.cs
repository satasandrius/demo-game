﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Shared.Dto.Items
{
    public class CreateItemDto
    {
        [JsonPropertyName("name")]
        public string Name { get; set; }
        [JsonPropertyName("description")]
        public string Description { get; set; }
        [JsonPropertyName("isFound")]
        public bool IsFound { get; set; }
        [JsonPropertyName("isUsed")]
        public bool IsUsed { get; set; }
    }
}
