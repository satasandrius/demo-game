﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess.Entities;
using Shared.Dto.PlayerItems;
using System.Text.Json.Serialization;

namespace Shared.Dto.Players
{
    public class PlayerDto
    {
        [JsonPropertyName("id")]
        public int Id { get; set; }

        [JsonPropertyName("name")]
        public string Name { get; set; }

        [JsonPropertyName("lucky")]
        public bool Lucky { get; set; }

        [JsonPropertyName("points")]
        public int Points { get; set; }
    }
}
