﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using Shared.Dto.PlayerItems;

namespace Shared.Dto.Players
{
    public class CreatePlayerDto
    {
        [JsonPropertyName("name")]
        public string Name { get; set; }

        [JsonPropertyName("lucky")]
        public bool Lucky { get; set; }

        [JsonPropertyName("points")]
        public int Points { get; set; }
    }
}
